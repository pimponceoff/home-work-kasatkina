package Hw.O.d2;

public class Multiply {
    public static int multiplyX(int ... x) {
        int res = 1;

        for (int i = 0; i < x.length; i++) {
            res *= x[i];
        }
        return res;
    }
}
