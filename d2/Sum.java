package Hw.O.d2;

public class Sum{

    public static int sumX(int ... x) {
        int res = 0;

        for (int i = 0; i < x.length; i++) {
            res += x[i];
        }
        return res;
    }
}
