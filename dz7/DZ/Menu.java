package DZ;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    public static void menu() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Выберите что хотите сделать: ");
        System.out.println("1 - добавить пользователя\n2 - найти пользователя по ID" +
                "\n3 - изменить параметры бользователя\n" +
                "4 - удалить пользователя ");
        int choise = scanner.nextInt();

        if(choise == 1){
            Map<Integer, User> users = new HashMap<>();
                UsersRepositoryFileImpl.create(users);

        } if(choise == 2) {
            System.out.println("Введите ID пользователя");
            int id = scanner.nextInt();
            UsersRepositoryFileImpl.findById(id);

        } if(choise == 3) {
            System.out.println("Введите ID пользователя");
            int id = scanner.nextInt();
            UsersRepositoryFileImpl.update(id);


        } if(choise == 4) {
            System.out.println("Введите ID пользователя для удаления");
            int id = scanner.nextInt();
            UsersRepositoryFileImpl.deleteUserById(id);


        }
    }
}
