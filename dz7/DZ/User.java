package DZ;

public class User {
    private int id;
    private String name;
    private String lastName;
    private int age;
    private boolean haveJob;

    public User(int id, String name, String lastName, int age, boolean haveJob) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.haveJob = haveJob;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return null;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isHaveJob() {
        return haveJob;
    }

    public void setHaveJob(boolean haveJob) {
        this.haveJob = haveJob;
    }

    @Override
    public String toString() {
        return id + "|"
                + name + "|"
                + lastName + "|"
                + age + "|"
                + haveJob + "|";
    }
}
