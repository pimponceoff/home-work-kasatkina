package DZ;

import java.util.Map;
import java.util.Scanner;

public class UserInfo {
    public static void addUserInfo(Map<Integer, User> users) {

        Scanner scanner = new Scanner(System.in);

        //:TODO оформить через map
        System.out.println("Введите свой id");
        int id = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Введите свою Фамилию и имя");
        String lastName = scanner.nextLine();
        String name = scanner.nextLine();

        System.out.println("Сколько вам лет?");
        int age = scanner.nextInt();

        System.out.println("Есть работа? (1 - Да/ 2 - Нет)");
        int choise = scanner.nextInt();

        if (choise == 1) {
            users.put(id, new User(id, name, lastName, age, true));
        } else {
            users.put(id, new User(id, name, lastName, age, false));
        }
    }
}

