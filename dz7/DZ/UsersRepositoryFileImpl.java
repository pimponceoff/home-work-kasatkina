package DZ;

import java.io.*;
import java.util.*;

public class UsersRepositoryFileImpl {
    public static void main(String[] args) {
        Menu.menu();

        int i = 0;
    }

    public static void findById(int id) {
        if (getUsers().get(id) == null) {
            System.out.println("пользователя с этим ID пока не существует");
        } else {
            System.out.println(getUsers().get(id));
        }
    }


    public static void deleteUserById(int id) {

        Scanner scanner = new Scanner(System.in);
        int choise;

        System.out.println(getUsers().get(id) + "\n вы точно хотите удалить пользователя?\n 1 - да\n 2 - нет");
        choise = scanner.nextInt();
        if (choise == 1) {
            String line = null;
            File sourceFile = new File("Names.txt");
            File outputFile = new File("Names2.txt");
            try {
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
                    BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

                    while ((line = reader.readLine()) != null) {
                        String[] info = line.split("\\|");
                        if (Integer.parseInt(info[0]) != id) {
                            writer.write(line);
                            writer.newLine();
                        }
                    }
                    reader.close();
                    writer.close();
                    sourceFile.delete();
                    outputFile.renameTo(sourceFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Вы передумали удалять");
            Menu.menu();
        }
    }


    public static void update(int id) {
        Scanner scanner = new Scanner(System.in);

        if ((getUsers().get(id)) == null) {
            System.out.println("Такого ID не существует");
            Menu.menu();
        }
        System.out.println(getUsers().get(id));

        String line = null;
        File sourceFile = new File("Names.txt");
        File outputFile = new File("Names2.txt");

        Map<Integer, User> users = new HashMap<>();
        System.out.println("хотите ввести новые данные? (1 - да/ 2 - нет)");
        int choise = scanner.nextInt();
        if (choise == 1) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
                BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

                while ((line = reader.readLine()) != null) {
                    String[] info = line.split("\\|");

                    if (id == Integer.parseInt(info[0])) {
                        getUsers().values().remove(id);
                        UpdateInfo.addUserInfo(users, id);
                        for (User user : users.values()) {
                            writer.write(user.toString() + "\n");
                        }
                    } else {
                        writer.write(line);
                        writer.newLine();
                    }
                }

                reader.close();
                writer.close();
                sourceFile.delete();
                outputFile.renameTo(sourceFile);

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("Вы передумали вносить изменения");
            Menu.menu();
        }
    }

    public static void create(Map<Integer, User> users) {

        UserInfo.addUserInfo(users);
        try (Writer writer = new FileWriter("Names.txt", true)) {
            for (User user : users.values()) {
                writer.write(user.toString() + "\n");
            }

        } catch (IOException exception) {
            throw new RuntimeException();
        }
    }

    public static Map<Integer, User> getUsers() {
        Map<Integer, User> users = new HashMap<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("Names.txt"))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] info = line.split("\\|");
                int id = Integer.parseInt(info[0]);
                String name = info[1];
                String lastName = info[2];
                int age = Integer.parseInt(info[3]);
                boolean haveJob = Boolean.parseBoolean(info[4]);
                users.put(id, new User(id, name, lastName, age, haveJob));
            }

        } catch (IOException e) {
            throw new RuntimeException();
        }
        return users;
    }
}
