package Hw;

interface Date {

    void AddDay();

    void GetString();

    static void main(String[] args) {

        HijraСalendar hijraСalendar = new HijraСalendar();

        System.out.println("************* Hijra");

        hijraСalendar.GetString();
        hijraСalendar.AddDay();

        GregorianСalendar gregorianСalendar = new GregorianСalendar();

        System.out.println("************* Gregorian");

        gregorianСalendar.GetString();
        gregorianСalendar.AddDay();

    }

}
