import java.io.*;
import java.util.Scanner;


public class File2 {
    public static void main(String[] args) {

        System.out.println("Если хотите удалить текст файла, напишите '.1' ");

        try {
            FileWriter fileWriter = new FileWriter("j2", true); // Если добавления в файл не требуются, то false


            Scanner input = new Scanner(System.in);

            String text = input.nextLine();
            fileWriter.write(text);

            fileWriter.flush();

            if(text.equals(".1")) {
                PrintWriter writer = new PrintWriter("j2");
                writer.print("");
                writer.close();
            }

        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }
}

