package Hw.O.d2;

public class Dividing {
    public static double dividingX(double ... x) {
        double res = 0.0;

        for (int i = 1; i < x.length; i += 1) {
            if(x[i] < x[i - 1]) {
                res = x[i - 1] / x[i];
            } else {
                break;
            }
        }

        return res;
    }
}
