package Hw.O.d2;

public class Factorial {
    public static int factorialX(int x) {
        int res = 1;

        for(int i = 1; i < x + 1; i++) {
            res *= i;
        }
        return res;
    }
}
