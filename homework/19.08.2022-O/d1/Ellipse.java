package Hw.O.d1;

public class Ellipse extends Figure {

    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public double getPerimetr() {
        return Math.PI * (x + y);
    }
}
