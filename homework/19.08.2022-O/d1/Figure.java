package Hw.O.d1;

public abstract class Figure {
    public int x;
    public int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    abstract double getPerimetr();

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
