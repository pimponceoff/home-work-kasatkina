package Hw.O.d1;

public class Main {
    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(1, 3);
        Rectangle rectangle = new Rectangle(2, 2);

        System.out.println(ellipse.getPerimetr());
        System.out.println(rectangle.getPerimetr());

        Square square = new Square(1,2);
        Circle circle = new Circle(10, 49);

        System.out.println("Square coordinates ***");
        square.move(10, 20);
        System.out.println("\nCircle coordinates ***");
        circle.move(10, 30);


    }
}