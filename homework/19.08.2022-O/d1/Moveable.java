package Hw.O.d1;

public interface Moveable {
    int x = 0;
    int y = 0;

    int[] move(int x, int y);

}
