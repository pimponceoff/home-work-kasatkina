package Hw.O.d1;

public class Rectangle extends Figure {
    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    double getPerimetr() {
        return x * 2 + y * 2;
    }
}
