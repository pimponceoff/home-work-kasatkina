package Hw.O.d1;

public class Square extends Rectangle implements Moveable{
    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public int[] move(int x, int y) {
        int[] s = new int[2];
        s[0] = x;
        s[1] = y;
        System.out.println("x: " + s[0]);
        System.out.println("y: " + s[1]);
        return s;
    }
}
