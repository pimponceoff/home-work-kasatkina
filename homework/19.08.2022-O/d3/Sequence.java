package Hw.O.d3.D3;


public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {

        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                System.out.println(array[i] + " - четное число");

                int sumNumbers = 0;

                while (array[i] > 0) {
                    sumNumbers += array[i] % 10;
                    array[i] /= 10;
                }

                if (sumNumbers % 2 == 0) {
                    System.out.println(sumNumbers + " - четная сумма цифр числа");
                    System.out.println("******************************************************\n");
                } else {
                    System.out.println(sumNumbers + " - нечетная сумма цифр числа");
                    System.out.println("******************************************************\n");
                }
            }


        }
        return array;
    }
}
