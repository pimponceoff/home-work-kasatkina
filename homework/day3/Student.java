package homework.day3;

public class Student implements Human {
    @Override
    public void see() {

    }

    @Override
    public void eat(Food food) {

    }

    @Override
    public void talk() {

    }

    @Override
    public void sleep() {
        throw new RuntimeException("Student should not sleep! only learn");
    }
}
